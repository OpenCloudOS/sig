## 【SIG名】
边缘计算 SIG
## 职责范围 Scope
主要是在把智慧工厂更多应用部署到企业中，矽甲科技有非常多项目落地实施经验，联通中讯院在5G网络切片和边缘计算做了很多设计，未来还是在行业垂直应用做更加细致工作。
智慧工厂是一个利用网络切片和边缘计算的一体化解决方案，智慧工厂可以提供多协议数据的采集，提供AI数据分析，数据筛选，包括一些监测视频的分析和处理，它的愿景是在保证客户应用运行质量的前提下实现极致的降本增效。
![输入图片说明](../images/sigedge1.png)
## SIG规划 Roadmap
- 基于 OpenCloudOS 的 CPU/Memory/Disk/Network 的隔离能力实现应用的 QoS；
- 基于内核指标观测应用之间的干扰；
- 基于内核的特性实现精细化的资源调度能力；
- 3月份-6月份 在某城市选择5家工厂做试点；
- 每个定期线下主题沙龙会；
## SIG成员 Maintainers
- 刘成钢（联通-中讯邮电设计院）
- 张涛（矽甲科技）
- 黄伟（矽甲科技）
- 汪志斌（矽甲科技）
- 李旭（矽甲科技）
- 郭大龙（矽甲科技）
- 赵健（腾讯）
- 王世伟（腾讯）
## 联络方式 Contact
- 微信群 （可选）
![输入图片说明](../images/%E5%BE%AE%E4%BF%A1%E5%9B%BE%E7%89%87_20230213135918.jpg)
- 邮件列表
  - 小组邮件列表
    - （由基础设施SIG分配后填写）
## 会议制度 Meetings
本兴趣小组的邮件列表会即时公布本小组的各项会议的具体细节。欢迎订阅我们的邮件列表。
- 例会（可选方式为周例会、双周例会或者月度例会。）
    - 会议周期：双周例会
    - 会议链接：（会议号 582 9093 8616）
- 定期沙龙： 线下主题沙龙会，每个月一次，邀请智慧工厂大咖来站台开会
## 如何加入SIG并参与贡献：
1. 注册Gitee/GitHub账号
2. 签署CLA
3. 找到对应SIG项目仓库地址
4. 参与贡献