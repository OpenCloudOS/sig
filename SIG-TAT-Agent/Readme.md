## TAT-Agent SIG

## 职责范围 Scope

自动化助手（TencentCloud Automation Tools，TAT）是由腾讯云推出的一款云服务器原生运维部署工具。TAT提供自动化的远程操作方式，可直接管理实例，批量执行 Shell，Powershell，Python等命令，轻松完成运行自动化运维脚本、轮询进程、安装或卸载软件、更新应用以及安装补丁等常见管理任务。

TAT 具有如下优点：

- 采用纯 rust 编写，内存占用少，内存安全，性能高；
- 可以运行于 CVM，Lighthouse，黑石2.0；
- 支持 TencentOS Server，及腾讯云官方提供的 Linux 发行版与 Windows 系统的公共镜像；
- 支持 Shell，PowerShell，Python 等；
- 无需登录云服务器、无需 sshd、无需外网、不受安全组影响；

借助自动化助手，用户能够以安全可靠的方式大规模远程管理实例，无需远程连接实例，也无需使用堡垒机和 SSH。用户可以自动批量执行常见管理任务，完成部署与运维任务。

![Manage Instances](https://foruda.gitee.com/images/1667293771737128624/e35fd6ed_8576736.png "Manage Instances.png")

自动化助手可以帮助用户在服务器中自动选择并部署操作系统和软件补丁，完成安装或者卸载软件、更新应用以及安装补丁等任务。

![Install Softwares](https://foruda.gitee.com/images/1667293804429725977/afc32983_8576736.png "Install Softwares.png")

TAT 与 OpenCloudOS 生态的集成，使得用户无需连接，即可快速创建和执行操作系统脚本命令，运行结果查看方便，省时高效，将极大降低服务器运维成本，有效提升 OpenCloudOS 使用体验，并进一步丰富 OpenCloudOS 生态。

## SIG规划 Roadmap

- 实现托管实例功能，完善对第三方云厂商的支持，提升用户体验，扩大 TAT 的适用范围。

## SIG成员 Maintainers

- 王守堰（腾讯）
- 刘洋（腾讯）
- 肖全举（腾讯）
- 赵晨程（腾讯）

## 联络方式 Contact

- 微信群 （可选）
- 邮件列表
  - 小组邮件列表
    - （由基础设施SIG分配后填写）

## 会议制度 Meetings

本兴趣小组的邮件列表会即时公布本小组的各项会议的具体细节。欢迎订阅我们的邮件列表。

- 例会（可选方式为周例会、双周例会或者月度例会。）
    - 会议周期：（请填写形式及时间）
    - 会议链接：（请填写）
- 定期沙龙（建议试行月度线上沙龙）

## 如何加入SIG并参与贡献：

1. 注册Gitee/GitHub账号
2. 签署CLA
3. 找到对应SIG项目仓库地址
4. 参与贡献
