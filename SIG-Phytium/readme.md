## Phytium SIG

## 职责范围 Scope
Phytium SIG对搭载Phytium高性能处理器的平台提供持续的内核适配支持与性能优化。
- 对齐OpenCloudOS Roadmap，在各发行版上使能Phytium硬件及相关功能。
- 为OpenCloudOS在Phytium平台上的高效、可靠的运行及完善、健全的功能提供支持。
- OC社区和飞腾共同组建联合实验室，向对方牵引适配伙伴，以共享资源，减轻认证厂商的重复认证流程为目的，在适配认证方面进行合作。
## SIG规划 Roadmap
- 提交并维护包括但不限于Phytium S5000C、E2000等高性能处理器平台的内核适配及修复补丁。
- 维护社区已提交的飞腾内核补丁代码。
- 发展OC-Phytium联合实验室。
## SIG成员 Maintainers
- 毛泓博
- 帅家坤
## 联络方式 Contact
- 微信群（可选）
- 邮件列表
  - 小组邮件列表
    - （由基础设施SIG分配后填写）
## 会议制度 Meetings
本兴趣小组的邮件列表会即时公布本小组的各项会议的具体细节。欢迎订阅我们的邮件列表。
- 例会（可选方式为周例会、双周例会或者月度例会。）
    - 会议周期：（请填写形式及时间）
    - 会议链接：（请填写）
- 定期沙龙（建议试行月度线上沙龙）
## 如何加入SIG并参与贡献：
1. 注册Gitee/GitHub账号
2. 签署CLA
3. 找到对应SIG项目仓库地址
4. 参与贡献