## 【Kubernetes SIG】

## 职责范围 Scope

Kubernetes 重新定义了整个云计算基础设施领域，已然成为容器技术的事实标准，是如今最热门的编排系统。K8S SIG 主要负责 Kubernetes 发行版和OpenCloudOS 的适配工作。Kubernetes 众多组件中中主要有 kubelet和容器引擎等多个部分对节点/内核有着高度依赖。K8S SIG 的工作除了去完善这些组件和OpenCloudOS的适配性之外，还包括如何深入挖掘OpenCloudOS提供的内核能力，去解决一些容器场景下遇到的棘手问题，包括但不限于：

- 内核如何提供更好的隔离性，减少容器间干扰
- 内核如何提供更多的容器监控指标，减少监控资源消耗量，提高容器的可观测性
- …

总之，K8S SIG 将会作为一个底座，来支持更多K8S 相关 SIG 登陆 OpenCloudOS。

## SIG规划 Roadmap

- 进行 Kubernetes 发行版和 OpenCloudOS 集成适配
- 进行 OpenCloudOS 作为 Kubernetes Node 适配工作以及参数优化
- 深入发掘 OpenCloudOS 特性对容器稳定性、性能以及可观测性优化的可能性，寻找更多合作机会

## SIG成员 Maintainers

- 李志宇
- 李波
- 唐聪
- 王超凡
- 程煜东

## 联络方式 Contact

- 微信群 （可选）
- 邮件列表
   - 小组邮件列表
      - （由基础设施SIG分配后填写）

## 会议制度 Meetings

本兴趣小组的邮件列表会即时公布本小组的各项会议的具体细节。欢迎订阅我们的邮件列表。

- 例会（可选方式为周例会、双周例会或者月度例会。）
   - 会议周期：（请填写形式及时间）
   - 会议链接：（请填写）
- 定期沙龙（建议试行月度线上沙龙）

## 如何加入SIG并参与贡献：

1. 注册Gitee/GitHub账号
2. 签署CLA
3. 找到对应SIG项目仓库地址
4. 参与贡献