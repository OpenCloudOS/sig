## 【Aeraki Mesh SIG】
## 职责范围 Scope
Aeraki [Air-rah-ki] Mesh 是一个 CNCF Sandbox 项目，为 Istio + Envoy 提供非 HTTP 协议的服务治理能力。Aeraki Mesh 可以帮助你在服务网格中管理任何七层协议。目前已经支持了 Dubbo、Thrit、Redis、Kafka、ZooKeeper 等开源协议，以及 trpc、qza、videopacket 等公司内的私有协议。你还可以使用 Aeraki Mesh 提供的 MetaProtocol 协议扩展框架来管理其他私有协议的七层流量。

![输入图片说明](https://foruda.gitee.com/images/1666250886003956588/39eb817f_7466754.png "aeraki-architecture (2).png")

Aeraki Mesh SIG 进行 Aeraki Mesh 与 OpenCloudOS 的集成，组织并协调小组成员共同推进 OpenCloudOS 与Service Mesh 之间共有软件生态的打造。


- Github: https://github.com/aeraki-mesh
- 官网: https://aeraki.net
- 典型案例: 
- 腾讯音乐：https://www.zhaohuabing.com/post/2022-04-26-aeraki-tencent-music-istiocon2022/
- 202
2冬奥会：https://www.zhaohuabing.com/post/2022-03-30-aeraki-mesh-winter-olympics-practice/


## SIG规划 Roadmap
- Aeraki Mesh SIG 进行 Aeraki Mesh 与 OpenCloudOS 的集成
- 组织并协调小组成员共同推进 OpenCloudOS 与Service Mesh 之间共有软件生态的打造。

## SIG成员 Maintainers
-  赵化冰
-  苗艳强
-  陈鹏
## 联络方式 Contact
- 微信群 （可选）
- 邮件列表
  - 小组邮件列表
    - （由基础设施SIG分配后填写）
## 会议制度 Meetings
本兴趣小组的邮件列表会即时公布本小组的各项会议的具体细节。欢迎订阅我们的邮件列表。
- 例会（可选方式为周例会、双周例会或者月度例会。）
    - 会议周期：（请填写形式及时间）
    - 会议链接：（请填写）
- 定期沙龙（建议试行月度线上沙龙）
## 如何加入SIG并参与贡献：
1. 注册Gitee/GitHub账号
2. 签署CLA
3. 找到对应SIG项目仓库地址
4. 参与贡献