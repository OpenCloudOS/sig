# SIG

#### 介绍
SIG全称Special Interest Group，针对特定的一个或多个技术主题而成立。社区按照不同SIG组织，以便更好管理和改善工作流程。SIG成员主导SIG的治理、推动交付成果输出，并争取让交付成果成为社区发行的一部分。
本仓库为OpenCloudOS SIG主仓库，方便开发者索引浏览

#### SIG Landscape

![OpenCloudOS SIG Landscape](images/SIG-Landscape.png)

欢迎任何人加入并参与贡献。您可以通过邮件列表与组内成员沟通，您在贡献的同时也将积累经验和提升影响力。

如果没有您需要的SIG，可以申请[创建SIG](https://gitee.com/OpenCloudOS/toc/tree/master/SIG)

#### 技术栈

| 架构体系/内核  | 核心基础/工具  | 云原生基础设施 | 图形/桌面 | 系统工具 | 中间件/语言/应用  |
| --------------- | --------------------------- | --------------- | ------------- | ------------- | --------------- |
| [SIG-kernel](https://gitee.com/OpenCloudOS/SIG-kernel "内核SIG") | [SIG-Storage](https://gitee.com/OpenCloudOS/sig-storage "存储SIG") | [SIG-CloudNative](https://gitee.com/OpenCloudOS/sig-cloud-native "云原生SIG") | [SIG-EX-NDE](https://gitee.com/OpenCloudOS/SIG-EX-NDE "超凡桌面SIG") | [SIG-ops](https://gitee.com/OpenCloudOS/SIG-ops "运维工具SIG") | [SIG-Microservice](./SIG-Microservice "微服务SIG") |
| [SIG-RISC-V-ARCH](https://gitee.com/OpenCloudOS/risc-v-arch-sig "RISC-V SIG") | [SIG-Network](https://gitee.com/OpenCloudOS/sig-network "网络SIG")  | [SIG-Kubernetes](./SIG-Kubernetes "Kubernetes SIG")  | [SIG-Lemon](./SIG-Lemon "柠檬SIG")  | [SIG-migrate](https://gitee.com/OpenCloudOS/SIG-ops "迁移工具SIG") |
| [SIG-DPU](https://gitee.com/OpenCloudOS/sig-dpu "DPU SIG") | [SIG-Virtualization](./SIG-Virtualization "虚拟化SIG") | [SIG-Servicemesh](./SIG-Servicemesh "Servicemesh SIG") | SIG-图形栈 | [SIG-TAT-Agent](./SIG-TAT-Agent "TAT-Agent SIG") |
| [SIG-Stream](https://gitee.com/OpenCloudOS/SIG-Stream "Stream SIG") | [SIG-System-Performance-Tool](https://gitee.com/OpenCloudOS/sig-system-performance-tool "系统性能工具SIG") | [SIG-Aerakimesh](./SIG-Aerakimesh "Aeraki SIG")  |
| [SIG-Intel](./SIG-Intel "Intel SIG") | [SIG-BPF](https://gitee.com/OpenCloudOS/sig-bpf "BPF SIG")| SIG-机密计算  |
| [SIG-Phytium](./SIG-Phytium "Phytium SIG")  | SIG-JDK|

#### 社区基础设施

| 版本发行 | 安全合规  | 测试  | 社区基础设施 |
| ---------------- | ------------------------- | ----------- | ------------- |
| [SIG-release](https://gitee.com/OpenCloudOS/SIG-release) | [SIG-Security](https://gitee.com/OpenCloudOS/SIG-Security)  | [SIG-Testing](https://gitee.com/OpenCloudOS/SIG-Testing) | [SIG-infra](https://gitee.com/OpenCloudOS/SIG-infra)  |
| SIG-Images 镜像| [SIG-compliance](https://gitee.com/OpenCloudOS/SIG-compliance)  | SIG-Bugzilla | SIG-WebSite|
| SIG-QA  | SIG-Vulnerability 公开漏洞 |  | [SIG-Docs](https://gitee.com/OpenCloudOS/sig-docs)|
| SIG-Version 版本  | SIG-国密|  | SIG-Translate |
| SIG-Package 打包  |  |  | SIG-Maillist  |
| SIG-Binary 二进制 |

#### 生态

| 行业生态 | 社区生态|
| ------------------------- | ------------ |
| [SIG-Ecological-Adaptation](https://gitee.com/OpenCloudOS/sig-ecological-adaptation "生态适配SIG") | [WG-Marketing](https://gitee.com/OpenCloudOS/SIG-Marketing "品宣WG") |
| [SIG-Standardization](./SIG-Standardization "标准化SIG") | WG-Operation |
| [SIG-Crane](./SIG-Crane "Crane SIG")  | WG-Outreach  |
| [SIG-Edge](./SIG-Edge "边缘计算SIG") | [SIG-Training](./SIG-Training "人才培养SIG") |
| SIG-TKE |
| SIG-TKEStack  |
| SIG-SuperEdge |

