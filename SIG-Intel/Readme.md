## 【SIG名】
Intel SIG
## 职责范围 Scope
Intel SIG聚焦Intel x86/x86-64架构兼容内核，操作系统和软件栈，对Intel平台及特性提供持续软件支持和优化。
- 对齐OC release, Linux upstream和Intel路线图，使能Intel硬件及相关特性。
- 调研关键特性用户场景和垂直软件栈，输出Intel特性相关解决方案和最佳用户实践。
- 联合OC SIG及Intel团队，以Intel SIG作为入口，宣传推广Intel技术及OC适配方案
## SIG规划 Roadmap
- 与Kernel SIG协同，在OC L1/L2/L3内核使能Intel硬件平台及相关特性。近期目标是实现对第四代英特尔至强可扩展处理器（Sapphire Rapids）的完整支持，中期目标是对Intel 2023-2025年度硬件使能和优化。
- 覆盖内核多个技术点，如CPU，Accelerator，Memory，Storage，Power，Perf，Security，RAS等，持续Intel架构的OS增强，补丁合入及性能/功耗优化。
- 对关键特性如加速器等，以内核能力为技术底座，调研用户场景，通过Intel SIG输出适配OC的软件栈和最佳实践。
## SIG成员 Maintainers
- 董宾
- 贾培
- 蒋彪
- 林青
- 宋有泉
- 夏开旭
## 联络方式 Contact
- 微信群 （可选）
- 邮件列表
  - 小组邮件列表
    - （由基础设施SIG分配后填写）
## 会议制度 Meetings
本兴趣小组的邮件列表会即时公布本小组的各项会议的具体细节。欢迎订阅我们的邮件列表。
- 例会（可选方式为周例会、双周例会或者月度例会。）
    - 会议周期：双周例会
    - 会议链接：（请填写）
- 定期沙龙（建议试行月度线上沙龙）
## 如何加入SIG并参与贡献：
1. 注册Gitee/GitHub账号
2. 签署CLA
3. 找到对应SIG项目仓库地址
4. 参与贡献
