## 【SIG-Servicemesh】
## 职责范围 Scope
Service Mesh 包含东西向（Sidecar）和南北向（云原生网关）流量的综合治理，是云原生时代微服务软件架构治理的基础技术。Service Mesh SIG 致力于通过 Service Mesh 实现应用与基础设施的解耦，加速应用和基础设施的独立演进，通过体系化的流量治理和观测能力，让分布式应用的开发、测试和运维产生质的飞跃。

![服务网格](https://foruda.gitee.com/images/1669727947727715589/7f539b7f_12119130.png "服务网格")

## SIG规划 Roadmap
- 适配主流Service Mesh产品（如istio）与OpenCloudOS的集成，例如编译环境、容器镜像、安装源等
- 调研与发掘OpenClousOS是否能在Service Mesh数据面上能够提供性能上的支持和优化
- 探索OpenCloudOS生态下的新型Service Mesh模式
## SIG成员 Maintainers
- 陈冠豪
- 赵化冰
- 田甜
## 联络方式 Contact
- 微信群 （可选）
- 邮件列表
  - 小组邮件列表
    - （由基础设施SIG分配后填写）
## 会议制度 Meetings
本兴趣小组的邮件列表会即时公布本小组的各项会议的具体细节。欢迎订阅我们的邮件列表。
- 例会（可选方式为周例会、双周例会或者月度例会。）
    - 会议周期：（请填写形式及时间）
    - 会议链接：（请填写）
- 定期沙龙（建议试行月度线上沙龙）
## 如何加入SIG并参与贡献：
1. 注册Gitee/GitHub账号
2. 签署CLA
3. 找到对应SIG项目仓库地址
4. 参与贡献
